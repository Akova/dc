<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\Company::create();
         \App\Models\Setting::create(['name'=>'lastid', 'value' => 1]);
         \App\Models\Setting::create(['name'=>'date', 'value' => "2021-05-23T12:47:13.000000Z"]);
    }
}
