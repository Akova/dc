<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['daily'],
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 20,
            'permission' => 0777,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => env('LOG_LEVEL', 'critical'),
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
        ],

        'emergency' => [
            'path' => storage_path('logs/laravel.log'),
        ],

        'loggly' => [
            'driver' => 'monolog',
            'handler' => Monolog\Handler\LogglyHandler::class,
            'with' => [
                'token' => env('LOGGLY_KEY'),
                'tag' => str_replace(' ', '_', env('APP_NAME') . '_' . env('APP_ENV')),
            ]
        ],

        'orders' => [
            'driver' => 'single',
            'path' => storage_path('logs/api/orders.log'),
            'level' => 'debug',
        ],

        'order-detail' => [
            'driver' => 'single',
            'path' => storage_path('logs/api/detail.log'),
            'level' => 'debug',
        ],

        'order-post' => [
            'driver' => 'single',
            'path' => storage_path('logs/api/post.log'),
            'level' => 'debug',
        ],

        'all-request' => [
            'driver' => 'daily',
            'path' => storage_path('logs/api/request/request.log'),
            'level' => 'info',
            'days' => 20,
            'permission' => 0777,
        ],

        'all-response' => [
            'driver' => 'daily',
            'path' => storage_path('logs/api/request/response.log'),
            'level' => 'info',
            'days' => 20,
            'permission' => 0777,
        ],

        'multi-all-request' => [
            'driver' => 'stack',
            'level' => 'info',
            'channels' => ['all-request'],
        ],

        'multi-all-response' => [
            'driver' => 'stack',
            'level' => 'info',
            'channels' => ['all-response'],
        ],

        'multi-orders' => [
            'driver' => 'stack',
            'channels' => ['orders'],
        ],

        'multi-order-detail' => [
            'driver' => 'stack',
            'channels' => ['order-detail'],
        ],

        'multi-order-post' => [
            'driver' => 'stack',
            'channels' => ['order-post'],
        ],
    ],

];
