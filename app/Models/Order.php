<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App\Models
 *
 * @property int $id
 * @property int $customer_id
 * @property int $company_id
 * @property int $billing_address_id
 * @property int $shipping_address_id
 * @property string $payment_method
 * @property string $shipping_method
 * @property string $type
 * @property float $total
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Order extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    public $fillable = [
        'id',
        'customer_id',
        'company_id',
        'billing_address_id',
        'shipping_address_id',
        'payment_method',
        'shipping_method',
        'type',
        'total',
        'created_at',
        'updated_at'
    ];

    //Relation
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function billingAddress()
    {
        return $this->belongsTo('App\Models\Address', 'billing_address_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shippingAddress()
    {
        return $this->belongsTo('App\Models\Address', 'shipping_address_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderItem()
    {
        return $this->hasMany('App\Models\OrderItem', 'order_id');
    }
}
