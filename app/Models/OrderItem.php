<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderItem
 * @package App\Models
 *
 * @property int $id
 * @property int $product_id
 * @property int $order_id
 * @property int $quantity
 * @property float $subtotal
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class OrderItem extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    public $fillable = [
        'id',
        'product_id',
        'order_id',
        'quantity',
        'subtotal',
        'created_at',
        'updated_at'
    ];

    //Relation
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }
}
