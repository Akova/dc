<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 * @package App\Models
 *
 *
 * @property int $id
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Company extends Model
{
    use HasFactory;

    //Relation
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order()
    {
        return $this->hasMany('App\Models\Order', 'company_id');
    }
}
