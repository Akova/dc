<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Customer extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    public $fillable = [
        'id',
        'name',
        'email',
        'phone',
        'created_at',
        'updated_at'
    ];

    //Relation
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order()
    {
        return $this->hasMany('App\Models\Order', 'customer_id');
    }
}
