<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Setting
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $value
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Setting extends Model
{
    use HasFactory;
}
