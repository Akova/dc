<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Address
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $line_1
 * @property string $line_2
 * @property string $city
 * @property string $country
 * @property string $state
 * @property string $postcode
 * @property DateTime $created_at
 * @property DateTime $updated_at
 *
 */
class Address extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    public $fillable = [
        'id',
        'name',
        'phone',
        'line_1',
        'line_2',
        'city',
        'country',
        'state',
        'postcode',
        'created_at',
        'updated_at'
    ];

    //Relation
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function billingAddress()
    {
        return $this->hasMany('App\Models\Order', 'billing_address_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shippingAddress()
    {
        return $this->hasMany('App\Models\Order', 'shipping_address_id');
    }
}
