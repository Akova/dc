<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $sku
 * @property float $price
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Product extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    public $fillable = [
        'id',
        'title',
        'description',
        'image',
        'sku',
        'price',
        'created_at',
        'updated_at'
    ];

    //Relation
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order()
    {
        return $this->hasMany('App\Models\Order', 'product_id');
    }
}
