<?php

namespace App\Console\Commands;

use App\Helpers\HttpClient;
use App\Jobs\DetailOrderJob;
use App\Models\Setting;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GetOrderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get orders for despatch cloud sample market commands ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::channel('multi-orders')->debug('Orders start');
        $working = true;
        $page = 1;
        $settings = Setting::all();
        $lastId = $settings[0]->value;
        $lastDate = $settings[1]->value;

        while ($working) {

            $this->info($page . '. page');
            $response = HttpClient::getRequest(['id' => $lastId, 'date' => $lastDate, 'page' => $page]);
            $page++;

            switch ($response->status()) {
                case 200:
                    if ($response->collect()->get('current_page') == $response->collect()['last_page']){
                        $working = false;
                    }

                    foreach ($response->collect()->get('data') as $order) {
                        if ($order['id'] > $settings[0]->value) {
                            $settings[0]->value = $order['id'];
                        }
                        DetailOrderJob::dispatch($order['id'])->delay(now()->addSeconds(20));
                        $this->info($order['id'].' Order add queue');
                    }

                    $settings[0]->save();
                    $settings[1]->value = now()->format("Y-m-d H:i:s");
                    $settings[1]->save();
                    break;
                case 500:
                    Log::channel('multi-orders')->debug('Server Error');
                    $this->error('Server Error');
                    sleep(5);
                    break;
                case 429:
                    Log::channel('multi-orders')->debug('Too many request');
                    $this->error('Too many request');
                    $working = false;
                    $response->throw();
                    break;
                default:
                    $working = false;
                    break;
            }
        }

        return;
    }
}
