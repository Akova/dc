<?php
namespace App\Helpers;

use Illuminate\Queue\Middleware\ThrottlesExceptions;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

/**
 * Class HttpClient
 * @package App\Helpers
 */
class HttpClient
{
    /**
     * @param string $url
     * @param array $params
     */
    protected static function requestLog($url, $params)
    {
        $message = [
            'requestUrl' => $url,
            'requestData' => $params
        ];
        Log::channel('multi-all-request')->info($url . ' request', $message);
    }

    /**
     * @param string $url
     * @param \Illuminate\Http\Client\Response $response
     */
    protected static function responseLog($url, $response)
    {
        $message = [
            'status' => $response->status(),
            'content' => $response->collect()
        ];
        Log::channel('multi-all-response')->info($url . ' response', $message);
    }

    /**
     * @param array $params
     * @param null $orderId
     * @return \Illuminate\Http\Client\Response
     */
    public static function getRequest($params = [], $orderId = null)
    {
        $url = is_null($orderId) ? env('API_URL') : env('API_URL') . '/' . $orderId;

        static::requestLog($orderId, $url, $params);

        $response = Http::withoutVerifying()
            ->withHeaders(['Accept' => 'application/json'])
            ->get($url, array_merge(['api_key' => env('API_KEY')], $params));

        static::responseLog($url, $response);

        return $response;
    }

    /**
     * @param array $params
     * @param $orderId
     * @return \Illuminate\Http\Client\Response
     */
    public static function postRequest($params = [], $orderId = '')
    {
        $url = env('API_URL') . '/' . $orderId;

        static::requestLog($url, $params);

        $response = Http::withoutVerifying()
            ->withHeaders(['Accept' => 'application/json'])
            ->get(env('API_URL') . '/' . $orderId, array_merge(['api_key' => env('API_KEY')], $params));

        static::responseLog($url, $response);

        return $response;
    }
}