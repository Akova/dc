<?php

namespace App\Jobs;

use App\Helpers\HttpClient;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\Middleware\ThrottlesExceptions;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class PostOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var int
     */
    public $tries = 3;

    /**
     * @var
     */
    protected $orderId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware()
    {
        return [(new ThrottlesExceptions(3, 1))->backoff(3)];
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addMinute();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = Order::find($this->orderId);
        if (!$order) {
            Log::channel('multi-order-post')->debug('Order not found' . $this->orderId);
            return;
        }
        Log::channel('multi-order-post')->debug('Order approved start');
        $response = HttpClient::postRequest(['type' => 'approved'], $this->orderId);

        switch ($response->status()) {
            case 200:
                $order->type = "approved";
                $order->save();
                break;
            case 500:
                Log::channel('multi-order-detail')->debug('Server Error');
                $response->throw();
                sleep(5);
                break;
            case 429:
                Log::channel('multi-order-detail')->debug('Too many request');
                $response->throw();
                break;
            default:
                break;
        }
    }
}
