<?php

namespace App\Jobs;

use App\Helpers\HttpClient;
use App\Models\Customer;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use Mockery\CountValidator\Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\Middleware\ThrottlesExceptions;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class DetailOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public $tries = 3;

    /**
     * @var
     */
    protected $orderId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }
    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware()
    {
        return [(new ThrottlesExceptions(3, 1))->backoff(3)];
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addMinute();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (Order::find($this->orderId)) {
            Log::channel('multi-order-detail')->debug('Order already exist' . $this->orderId);
            return;
        }

        Log::channel('multi-order-detail')->debug($this->orderId.' Order Detail');
        $response = HttpClient::getRequest([], $this->orderId);

        try {
            switch ($response->status()) {
                case 200:
                    $customer = $response->collect()->get('customer');
                    if (isset($customer)) {
                        $customer = Customer::updateOrCreate($customer);
                        Log::channel('multi-order-detail')->debug('Customer', $customer->toArray());
                    }
                    $billingAddress = $response->collect()->get('billing_address');
                    if (isset($billingAddress)) {
                        $billingAddress = Address::updateOrCreate($billingAddress);
                        Log::channel('multi-order-detail')->debug('Billing Address', $billingAddress->toArray());
                    }
                    $shippingAddress = $response->collect()->get('shipping_address');
                    if (isset($shippingAddress)) {
                        $shippingAddress = Address::updateOrCreate($shippingAddress);
                        Log::channel('multi-order-detail')->debug('Shipping Address', $shippingAddress->toArray());
                    }
                    if (isset($customer) && isset($billingAddress) && isset($shippingAddress)) {
                        $order = Order::updateOrCreate($response->collect()->except(['customer', 'billing_address', 'shipping_address', 'order_items'])->all());
                        Log::channel('multi-order-detail')->debug('Order', $order->toArray());
                    }
                    $orderItems = $response->collect()->get('order_items');
                    if (isset($orderItems)) {
                        foreach ($orderItems as $orderItem) {
                            $product = Product::updateOrCreate($orderItem['product']);
                            Log::channel('multi-order-detail')->debug('Product', $product->toArray());
                            unset($orderItem['product']);
                            $orderItem = OrderItem::updateOrCreate($orderItem);
                            Log::channel('multi-order-detail')->debug('Order Item', $orderItem->toArray());
                        }
                    }
                    PostOrderJob::dispatch($this->orderId)->onQueue('high');
                    break;
                case 500:
                    Log::channel('multi-order-detail')->debug('Server Error');
                    $response->throw();
                    sleep(5);
                    break;
                case 429:
                    Log::channel('multi-order-detail')->debug('Too many request');
                    $response->throw();
                    break;
                default:
                    break;
            }
        } catch (Exception $e) {
            Log::channel('multi-order-detail')->debug('Error', $e);
        }
    }
}
